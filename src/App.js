import "./App.css";

import React from "react";
import { Routes, Route } from "react-router-dom";
import Home from "./Components/Home";
import SignUp from "./Components/SignUp";
import Login from "./Components/Login";
import DashBoard from "./Components/DashBoard";
import {AuthProvider} from './Components/Auth';
import ProtectedComponent from "./Components/ProtectedComponent";

function App() {
  return(
    <>
  
   <AuthProvider>
   <Routes>
      <Route path="/" element={<Home />}></Route>
      <Route path="signup" element={<SignUp />}></Route>
      <Route path="login" element={<Login />}></Route>
     
      <Route path="dashboard" element={
          <ProtectedComponent>
          <DashBoard />
      </ProtectedComponent>
      }></Route>
    </Routes>
   </AuthProvider>

  </>
  )
 
}

export default App;
