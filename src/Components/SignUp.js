import React from 'react';
import {useForm} from 'react-hook-form'
import {Link,useNavigate} from 'react-router-dom'

function SignUp (){
    const navigate = useNavigate()
    
    const {register,handleSubmit,formState: {errors},reset} = useForm();

    const onSubmit = (data) => {
        console.log(data);
        const encdata = {username: window.btoa(data.username),email: window.btoa(data.email),password: window.btoa(data.password),confirm_password: window.btoa(data.confirm_password),phone: window.btoa(data.phone)}
        localStorage.setItem("item",JSON.stringify(encdata))
        console.log(data);
        reset();
        navigate('/login')
    }

 
    return(
        <div>
            <div>
                <h2>SignUp page</h2>
            </div>

           
            <div>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className='user-name'>
                    <label>Username: </label><br />
                        <input type="text" {...register("username",{required: "username is required"})}></input><br />
                        {errors.username && (<small className="error_msg">{errors.username.message}</small>)}<br /><br/>
                    </div>

                    <div>
                        <label>Email : </label><br/>
                        <input type="text" {...register("email",{required: "Email is required",
                        pattern: {
                            value:  /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                            message: "Invalid email address",
                        }
                        })}>
                        </input><br/>
                        {errors.email && (<small className="error_msg">Email is required</small>)}<br /><br/>
                    </div>


                  <div>
                    <label>Password</label><br />
                    <input type="text" {...register("password",{required: "password is required"})}></input><br />
                    {errors.password && (<small className='error_msg'>{errors.password.message}</small>)}<br /><br/>
                  </div>

                  <div>
                      <label>Confirm Password </label><br/>
                      <input type="text" {...register("confirm_password",{required: 'This field is required'})}></input><br />
                      {errors.confirm_password && (<small className='error_msg'>{errors.confirm_password.message}</small>)}<br /><br/>
                  </div>

                    <div>
                        <label>Phone no :</label><br/>
                        <input type="phone" {...register("phone_no",{required: "phone number is required",
                    pattern:{
                        value:  /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/,
                        message: "Invalid phone no"
                    }})}></input><br/>
                        {errors.phone_no && (<small className='error_msg'>{errors.phone_no.message}</small>)}<br /><br/>
                    </div>

                   
                    
                    <div>
                      <button type="submit" className='sign_up'>Sign Up</button>
                    </div>

                    <div>
                        <p>Already have an account ?  <Link to="/login" className='form-links'>Login</Link></p>
                    </div>
                   
                </form>
            </div>
         
        </div>
    
    )
}

export default SignUp;