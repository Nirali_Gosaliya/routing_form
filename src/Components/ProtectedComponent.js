import React from 'react'
import { Navigate, useLocation, useNavigate } from 'react-router-dom'
import Auth from './Auth'

function ProtectedComponent({children}) {
    const location = useLocation();
    const auth = Auth();

    if(!auth.user){
        return <Navigate to='/login' state ={{path: location.pathname}} />
    }
  return children
}

export default ProtectedComponent;