import React from 'react'
import style from '../assets/CSS/style.css'
import {Link} from 'react-router-dom';


function Home() {
  return (
   <div>
       <div>
           <p className='home-title'>Home Page</p>
       </div>

       <div className='links'>
            <Link to="signup" className='form-links'>Sign Up</Link><br /><br />
           <Link to="login" className='form-links'>Login</Link><br /><br />
           <Link to="dashboard" className='form-links'>Dashboard</Link><br /><br />
       </div>
   </div>
  )
}

export default Home;