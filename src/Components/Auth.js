import React from 'react'
import {useState,createContext,useContext} from 'react';

const AuthContext = createContext(null);

export const AuthProvider = ({children}) => {
    const [user,setUser] = useState(null);

    const login = user => {
        setUser(user)
    }
  
    return(
        <AuthContext.Provider value={{user,login}}>
            {children}
        </AuthContext.Provider>
    )
}

function Auth() {
  return useContext(AuthContext)
}

export default Auth;