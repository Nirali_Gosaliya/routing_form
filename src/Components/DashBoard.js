import React from 'react';
import {useNavigate} from 'react-router-dom';
import Auth from './Auth';

function DashBoard() {
  const navigate = useNavigate();
  const auth = Auth();
 
  return (
    <div>
      <p className='welcome_dashboard'>
      Welcome to dashboard <br /> <br />
      {auth.user}
      </p>
     
    </div>
  )
}

export default DashBoard;