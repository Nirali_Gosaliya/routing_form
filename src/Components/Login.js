import React from "react";
import { useForm } from "react-hook-form";
import { Link, useNavigate } from "react-router-dom";
import Auth from "./Auth";

function Login() {
  const navigate = useNavigate();
  const auth = Auth();

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm();

  const onSubmit = (data) => {
    console.log(data);
    const store = JSON.parse(localStorage.getItem("item"));

    if (store.username === window.btoa(data.username)) {
      auth.login(data.username);
     
      reset();
      navigate("/dashboard");
    }
    else{
      alert("does not match")
    }
  };
  return (
    <div>
      <div>Login page</div>
      <br />
      <br />

      <div>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="user-name">
            <label>Username: </label>
            <br />
            <input
              type="text"
              {...register("username", { required: "username is required" })}
            ></input>
            <br />
            {errors.username && (
              <small className="error_msg">{errors.username.message}</small>
            )}
            <br />
            <br />
          </div>

          <div>
            <div>
              <label>Password</label>
              <br />
              <input
                type="text"
                {...register("password", { required: "password is required" })}
              ></input>
              <br />
              {errors.password && (
                <small className="error_msg">{errors.password.message}</small>
              )}
              <br />
              <br />
            </div>

            <div>
              <button type="submit" className="login_button">
                Login
              </button>
            </div>

            <div>
              <p>
                Dont Have an account ? <Link to="/signup">Sign Up</Link>
              </p>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Login;
